# FLAWS
## Fortran's Long Awaited Windowing System

## Introduction
Too often have I heard "Fortran's a dead language", or "Fortran's only good at
number-crunching", or "Why don't you get on with your work and stop wasting your
time making Fortran do what it's not designed for"; I say enough!

FLAWS (Fortran's Long Awaited Windowing System) aims to completely replace the
X11/Wayland GUI interface for Linux, using Fortran to design a GUI interface to
Linux from the `/dev/` up.

## Implementation
FLAWS will use the `/dev/inputs` to read from the keyboard & mouse, and write to
`/dev/fb<x>` in order to allow interaction with a graphical interface. Within
this, GUI applications will probably have to be written from scratch, or ported
from X11.

Beyond that... TBD.

## Goals
This project will be deemed a "success" if:

  - Windows can be drawn/manipulated using the mouse
  - Text can be displayed, allowing for a virtual terminal to be implemented
  - Images can be rendered.

Beyond that, anything's possible, right?

## Requirements
  - A compatible Linux system (Testing is done in Ubuntu 18.04)
  - A Fortran compiler (tested using `gfortran 7.4.0`, will likely need 03 features)
  - Superuser privileges on the machine (to read/write to `/dev/`
  - Either:
    - A complete inability to judge what is a sensible thing to do, or
    - A stubborn willingness to do what is clearly mad.
