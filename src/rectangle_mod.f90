module rectangle_mod
!==============================================================================#
! RECTANGLE_MOD
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-06-11
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use drawable_mod
  implicit none

  private

  type, public, extends(drawable) :: rectangle
    integer         :: border_width = 1
    integer(kind=1) :: border_color(4) = [  0,   0,   0, 127]
    integer(kind=1) :: body_color(4)   = [127, 127, 127, 127]
  contains
    procedure :: destroy => rectangle_destroy
    procedure :: draw    => rectangle_draw
    procedure :: create_rectangle => rectangle_create
    procedure :: set_body_color => rectangle_body_color
  end type rectangle

contains

  subroutine rectangle_create(self, id, parent, location, dimensions, &
                                    border_width, border_color, body_color)
    class(rectangle) :: self
    character(len=*), intent(in) :: id
    class(drawable),  intent(in), pointer :: parent
    integer,          intent(in) :: border_width
    integer(kind=1),  intent(in) :: border_color(4)
    integer(kind=1),  intent(in) :: body_color(4)
    integer,          intent(in) :: location(2)
    integer,          intent(in) :: dimensions(2)

    call self%create_drawable(id, parent, location, dimensions)

    self%border_width = border_width
    self%border_color = border_color
    self%body_color   = body_color

  end subroutine rectangle_create

  subroutine rectangle_destroy(self)
    class(rectangle) :: self

    self%border_width = 1
    self%border_color = [  0,   0,   0, 127]
    self%body_color   = [127, 127, 127, 127]
    call self%destroy_drawable()

  end subroutine rectangle_destroy

  subroutine rectangle_draw(self)
    class(rectangle) :: self

    integer :: i, j

    do j=1, self%border_width
      do i=1, self%dimensions(1)
        self%buffer(:,i,j) = self%border_color
      end do
    end do
    do j=self%dimensions(2)-self%border_width+1, self%dimensions(2)
      do i=1, self%dimensions(1)
        self%buffer(:,i,j) = self%border_color
      end do
    end do

    do j=1, self%dimensions(2)
      do i=1, self%border_width
        self%buffer(:,i,j) = self%border_color
      end do
      do i=self%dimensions(1)-self%border_width+1, self%dimensions(1)
        self%buffer(:,i,j) = self%border_color
      end do
    end do

    do j=self%border_width+1, self%dimensions(2)-self%border_width
      do i=self%border_width+1, self%dimensions(1)-self%border_width
        self%buffer(:,i,j) = self%body_color
      end do
    end do

    call self%draw_drawable()

    end subroutine rectangle_draw

    subroutine rectangle_body_color(self, color)
      class(rectangle) :: self
      integer(kind=1), intent(in) :: color(4)

      self%body_color = color
    end subroutine rectangle_body_color 

end module rectangle_mod
