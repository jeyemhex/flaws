module cursor_mod
!==============================================================================#
! CURSOR_MOD
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-06-13
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use drawable_mod
  use rectangle_mod
  implicit none

  private

  type, public, extends(drawable) :: cursor
    integer :: dev
    integer(kind=1) :: buttons(1)
    integer :: sensitivity
  contains
    procedure :: read_input => cursor_read_input
    procedure :: destroy => cursor_destroy
    procedure :: create_cursor => cursor_create
  end type cursor

contains

  subroutine cursor_create(self, id, parent, dev, sensitivity)
    class(cursor), target :: self
    character(len=*),  intent(in) :: id
    class(drawable),   intent(in), pointer :: parent
    character(len=*),  intent(in) :: dev
    integer,           intent(in) :: sensitivity
    type(rectangle), pointer :: sq

    call self%create_drawable(id, parent, parent%dimensions(:)/2-8, [16,16])

    allocate(sq)
    call sq%create_rectangle( id           = trim(id) // "::icon",                 &
                              parent       = self,              &
                              location     = [1, 1],       &
                              dimensions   = [16, 16],       &
                              border_width = 2,                &
                              border_color = int([  0, 0, 0, 127], kind=1), &
                              body_color   = int([127, 127, 127, 127], kind=1)  &
                  )
    


    self%sensitivity = sensitivity

    self%dev = 21
    open(UNIT=self%dev, FILE=dev, FORM="unformatted", ACCESS="stream")

  end subroutine cursor_create

  subroutine cursor_destroy(self)
    class(cursor) :: self

    close(self%dev)

    call self%destroy_drawable()

  end subroutine cursor_destroy
  
  subroutine cursor_read_input(self)
    class(cursor) :: self
    integer(kind=1) :: bytes(3)

    read(self%dev) bytes

    if (bytes(1) == 9) then
      self%buttons(1) = 1
    else if (bytes(1) == 8) then
      self%buttons(1) = 0
    end if

    call self%move(bytes(2:3)*[1,-1]*self%sensitivity)

    select type(sq => self%children)
    type is (rectangle)
      if (self%buttons(1) == 1) then
        call sq%set_body_color(int([0, 127, 0, 127], kind=1))
      else
        call sq%set_body_color(int([127, 127, 127, 127], kind=1))
      end if
    end select

  end subroutine cursor_read_input

end module cursor_mod
