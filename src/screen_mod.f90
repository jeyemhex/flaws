module screen_mod
!==============================================================================#
! SCREEN
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-06-11
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use drawable_mod
  implicit none

  private

  type, public, extends(drawable) :: screen
    character(len=16) :: fb_name
    integer(kind=1)   :: bg_color(4) = int([0, 0, 0, 0], kind=1)
    integer           :: fb = 11
    integer           :: fps
  contains
    procedure :: create_screen  => screen_create
    procedure :: destroy => screen_destroy
    procedure :: draw    => screen_draw
  end type screen

contains

  ! A routine to create a screen object
  subroutine screen_create(self, id, framebuffer, dimensions, bg_color, fps)
    class(screen) :: self
    character(len=*),  intent(in) :: id
    integer,           intent(in) :: dimensions(2)
    character(len=*),  intent(in) :: framebuffer
    integer(kind=1),   intent(in) :: bg_color(4)
    integer,           intent(in) :: fps

    type(drawable), pointer :: null_ptr
    integer :: i, j
    integer :: ierr
    print *, "Size(1) = [", dimensions, "]"

    null_ptr => null()
    call self%create_drawable( id         = id,         &
                               parent     = null_ptr,   &
                               location   = [0,0],      &
                               dimensions = dimensions  &
                             )

    do j=1, dimensions(2)
      do i=1, dimensions(1)
        self%buffer(:,i,j) = bg_color
      end do
    end do

    self%fb_name  = framebuffer
    self%fps      = fps
    self%bg_color = bg_color

    open(UNIT=self%fb, FILE=self%fb_name, FORM="unformatted", ACCESS="stream", iostat=ierr)
    if (ierr /= 0) stop "Unable to open framebuffer " // trim(framebuffer)

  end subroutine screen_create

  ! A routine to destroy a screen object
  subroutine screen_destroy(self)
    class(screen) :: self

    integer :: ierr

    close(self%fb, iostat=ierr)
    if (ierr /= 0) stop "Unable to close framebuffer " // self%fb_name

    call self%destroy_drawable()

  end subroutine screen_destroy

  ! a routine to draw everything within a screen to that screen's device
  subroutine screen_draw(self)
    class(screen) :: self

    character(len=8) :: fps_str
    integer :: i, j

    do j=1, self%dimensions(2)
      do i=1, self%dimensions(1)
        self%buffer(:,i,j) = self%bg_color
      end do
    end do

    call self%draw_drawable()

    rewind(self%fb)
    write(self%fb) self%buffer

    write(fps_str, '(1f8.6)') 1.0 / self%fps
    call system("sleep " // fps_str)

  end subroutine screen_draw

end module screen_mod
