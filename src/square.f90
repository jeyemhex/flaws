program square
!==============================================================================#
! SQUARE
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-06-11
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use screen_mod
  use cursor_mod
  implicit none

  type(screen), pointer :: fb0
  type(cursor), pointer :: mouse
  integer :: i


  allocate(fb0)
  call fb0%create_screen( id          = "fb0",                             &
                          framebuffer = "/dev/fb0",          &
                          dimensions  = [1920, 1080],        &
                          bg_color    = int([50, 40, 30, 127], kind=1), &
                          fps         = 60                   &
                 )

  allocate(mouse)
  call mouse%create_cursor( id     = "mouse1",            &
                            parent = fb0,                 &
                            dev    = "/dev/input/mouse1", &
                            sensitivity = 5               &
                          )
 
  call fb0%draw()
  do
    call mouse%read_input()
    call fb0%draw()
    if (mouse%buttons(1) == 1) exit 
  end do

  call fb0%destroy()

  deallocate(fb0, mouse)

end  program square
