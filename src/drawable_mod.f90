module drawable_mod
!==============================================================================#
! DRAWABLE
!------------------------------------------------------------------------------#
! DESCRIPTION
!   Class containing all the generic data/operations that can needed for a
!   drawable object
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-06-10
!------------------------------------------------------------------------------#
! self code is distributed under the MIT license.
!==============================================================================#
  implicit none

  private

  type, public :: drawable
    character(len=1024)           :: id
    class(drawable), pointer      :: parent   => null()
    class(drawable), pointer      :: children => null()
    class(drawable), pointer      :: next     => null()
    integer(kind=1), allocatable  :: buffer(:,:,:)
    integer                       :: location(2) = [0,0]
    integer                       :: dimensions(2) = [0,0]
  contains
    procedure :: create_drawable  => drawable_create
    procedure :: destroy_drawable => drawable_destroy
    procedure :: draw_drawable    => drawable_draw
    procedure :: destroy => drawable_destroy
    procedure :: move => drawable_move
    procedure :: draw => drawable_draw
  end type drawable

contains

  ! A routine to allocate the memory needed for a drawable object
  subroutine drawable_create(self, id, parent, location, dimensions)
    class(drawable), target :: self
    character(len=*),intent(in) :: id
    class(drawable), intent(in),pointer :: parent
    integer,         intent(in) :: location(2)
    integer,         intent(in) :: dimensions(2)
    integer :: ierr

    class(drawable), pointer :: child

    self%parent     => parent

    if (associated(self%parent)) then
      if (associated(self%parent%children)) then
        child => parent%children
        do while (associated(child))
          child => child%next
        end do
        child%next => self
      else 
        parent%children => self
      end if
    end if

    self%id         = id
    self%location   = location
    self%dimensions = dimensions

    allocate(self%buffer(4, dimensions(1), dimensions(2)))
    self%buffer(:,:,:) =  0

  end subroutine drawable_create

  ! A routine to deallocate memory needed for a drawable object and its children
  recursive subroutine drawable_destroy(self)
    class(drawable) :: self

    if (associated(self%children)) then
      call self%children%destroy()
      self%children => null()
    end if

    if (associated(self%next)) then
      call self%next%destroy()
      self%next => null()
    end if

    deallocate(self%buffer)
    self%location   = [0,0]
    self%dimensions = [0,0]

  end subroutine drawable_destroy

  ! A routine to draw a drawable object and its children
  recursive subroutine drawable_draw(self)
    class(drawable) :: self
    class(drawable), pointer :: child
    integer :: i, j

    child => self%children
    do while (associated(child))
      call child%draw()
      do j=1, child%dimensions(2)
        do i=1, child%dimensions(1)
          self%buffer(:,child%location(1)+i-1,child%location(2)+j-1) = child%buffer(:,i,j)
        end do
      end do
      child => child%next
    end do

  end subroutine drawable_draw

  subroutine drawable_move(self, disp)
    class(drawable) :: self
    integer, intent(in) :: disp(2)
    integer :: i

    do i=1,2
      if(self%location(i) + self%dimensions(i) + disp(i) > self%parent%dimensions(i)) then
        self%location(i) = self%parent%dimensions(i) - self%dimensions(i)+1
      else if (self%location(i)+disp(i) <= 0) then
        self%location(i) = 1
      else
        self%location(i) = self%location(i) + disp(i)
      end if
    end do

  end subroutine drawable_move

end module drawable_mod
